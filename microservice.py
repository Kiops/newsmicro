import redis
import time
import datetime
import smtplib
# from django.template import Template, Context
import django
from django.template.loader import get_template
from django.template import Context
from django.conf import settings


class Microservice:
    posts = []
    emails = ["sir.kiops@yandex.ru"]
    # delay = datetime.timedelta(minutes= 5)
    delay = datetime.timedelta(seconds=10)
    oldestPostTime = datetime.datetime.now()

    def __init__(self):
        TEMPLATES = [
            {
                'BACKEND': 'django.template.backends.django.DjangoTemplates',
                'DIRS': ['./'],
            }
        ]
        settings.configure(TEMPLATES=TEMPLATES)
        django.setup()
        self.t = get_template("templateForEmail.html")

    def addPost(self, content):
        if len(self.posts) == 0:
                self.oldestPostTime = datetime.datetime.now()
        post = dict(header=content[0], content_text=content[1],
                    pub_date=content[2], image_link=content[3],
                    tags=content[4])
        self.posts.append(post)
        print(self.posts)


    def RedisCheck(self):
        try:
            r = redis.StrictRedis(host='127.0.0.1', port=6379)
            p = r.pubsub()
            p.subscribe('startScripts')

            while True:
                print("Waiting For redisStarter...")
                message = p.get_message()
                if ((datetime.datetime.now() - self.oldestPostTime > self.delay) and
                len(self.posts) > 0):
                    self.sendEmail()
                if message:
                    command = str(message['data'])
                    if (str("!@#") in command):
                        command = command[:-1] #trim last symbol
                        separated = command.split('!@#')
                        separated = separated[1:]
                        self.addPost(separated)


                time.sleep(1)

            print("Permission to start...")

        except Exception as e:
            print(e)
            # self.RedisCheck()

    def sendEmail(self):
        server = smtplib.SMTP('smtp.gmail.com', 587)
        server.connect("smtp.gmail.com", 587)
        # server.ehlo()
        server.starttls()
        # server.ehlo()
        server.login("newsmicrosmtp", "newsmicrosmtp12345")
        for email in self.emails:
            server.sendmail("kiopscool@gmail.com", email, self.render())
        server.quit()


    def render(self):
        c = {"latest_posts": self.posts}
        result = self.t.render(c)
        self.posts = []
        return result



if __name__ == '__main__':
    m = Microservice()
    m.RedisCheck()

