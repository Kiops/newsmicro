from django.db import models
from django.db.models.signals import post_save
import redis


r = redis.StrictRedis(host='127.0.0.1', port=6379)
p = r.pubsub()

def my_callback(sender, **kwargs):
    # Workaround to signal being emitted twice on create and save
    if 'created' in kwargs:
        if kwargs['created']:
            return
    post = Post.objects.order_by('-pub_date')[0]
    delim = '!@#'
    message = delim + post.header + delim + \
              post.content_text + delim + str(post.pub_date) \
              + delim + post.image_link + delim + post.tags

    try:
        r.publish('startScripts', message)
    except Exception as e:
        print("Fucking fuck")
    print("K, done")

class Post(models.Model):
    header = models.CharField(max_length=255)
    content_text = models.CharField(max_length=4000)
    pub_date = models.DateTimeField('date published')
    image_link = models.CharField(max_length=1024, null=True)
    tags = models.CharField(max_length=255, null=True)
    post_save.connect(my_callback)
# Create your models here.
