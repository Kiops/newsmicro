
# Create your views here.
from django.views.generic import TemplateView
from django.http import HttpResponse
from django.shortcuts import render
from django.template import loader
from .models import Post
from .forms import PostForm


# def index(request):
#     latest_posts = Post.objects.all()
#     template = loader.get_template('news/index.html')
#
#     for post in latest_posts:
#         post.tags = post.tags.split(' ')
#
#     context = {"latest_posts": latest_posts}
#     for post in latest_posts:
#         print('mark', post)
#     return HttpResponse(template.render(context, request))

class Index(TemplateView):
    template_name = 'news/index.html'

    def get(self, request):
        # form = PostForm()

        latest_posts = Post.objects.order_by('-pub_date')
        for post in latest_posts:
            post.tags = post.tags.split(' ')

        context = {'latest_posts': latest_posts}
                   # 'form': form}
        return render(request=request, template_name=self.template_name,
                      context=context)
